package in.keepgrowing.jwtspringbootangularscaffolding.entities;

import lombok.Data;

import java.util.Objects;
import java.util.Set;

@Data
public class EventType {
    private int id;
    private String name;
    private Set<Attribute> attributes;
    private Set<Attribute> requiredAttributes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventType eventType = (EventType) o;
        return id == eventType.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "EventType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
