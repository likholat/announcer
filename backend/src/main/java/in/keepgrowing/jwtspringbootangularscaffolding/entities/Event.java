package in.keepgrowing.jwtspringbootangularscaffolding.entities;

import lombok.Data;

import java.util.Map;
import java.util.Objects;

@Data
public class Event {
    private int id;
    private String name;
    private EventType eventType;
    private Map<Integer, String> valuesMap;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return id == event.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}


