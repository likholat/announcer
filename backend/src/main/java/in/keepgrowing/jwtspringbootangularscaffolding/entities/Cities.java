package in.keepgrowing.jwtspringbootangularscaffolding.entities;

import lombok.Data;

import java.util.Map;
import java.util.Set;

@Data
public class Cities {
    private static final int id = 32;
    private static final int attributeCityId = 2;
    private Set<String> cityNames;

    public static int getId() {
        return id;
    }

    public static int getAttributeCityId(){
        return attributeCityId;
    }

}
