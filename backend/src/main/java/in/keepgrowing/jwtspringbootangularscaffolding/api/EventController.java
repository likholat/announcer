package in.keepgrowing.jwtspringbootangularscaffolding.api;


import in.keepgrowing.jwtspringbootangularscaffolding.entities.Event;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventForUserRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventTypeRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.user.UserService;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.UserInformationRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/api/event")
public class EventController {
private UserService userService;
private final EventRepository eventRepository;
private final UserInformationRepository userInformationRepository ;


    public EventController(EventRepository eventRepository, EventTypeRepository eventTypeRepository, EventForUserRepository eventForUserRepository, UserService userService, UserInformationRepository userInformationRepository) {
    this.eventRepository = eventRepository;
        this.userService = userService;
        this.userInformationRepository = userInformationRepository;
    }

  @GetMapping
  @CrossOrigin(origins = "*")
  public List<Event> getSimpleEventList() {
    return new ArrayList<>(eventRepository.getEvents());
  }

  @GetMapping("/byDate")
  public List<Event> getSimpleEventListByDate() {
      Set<Event> events = eventRepository.getEvents();
      return new ArrayList<>(eventRepository.sortEventsByDate(events));
  }

    @GetMapping("/byCity")
    public List<Event> getActualEventsForCity(){
        String userCity;
        Long userId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            if (userService.findByUsername(username).isPresent()) {
                userId = userService.findByUsername(username).get().getId();
                userCity = userInformationRepository.getCity(userId);
                if(!userCity.equals(""))
                    return new ArrayList<>(eventRepository.getEventsByCityAndDate(userCity));
                else {
                    return new ArrayList<>(getSimpleEventListByDate());
                }
            }
            return new ArrayList<>();
        }
        return new ArrayList<>();

    }


  @GetMapping("/{id}")
  @ResponseBody
  @CrossOrigin(origins = "*")
  public Event getDetailedEvent(@PathVariable String id) {
    try {
      int intId = Integer.parseInt(id);
      return eventRepository.getEventById(intId);
    } catch (NumberFormatException ex) {
      throw new NumberFormatException("Wrong eventId");
    }
  }

}
