package in.keepgrowing.jwtspringbootangularscaffolding.api;

import in.keepgrowing.jwtspringbootangularscaffolding.entities.Event;
import in.keepgrowing.jwtspringbootangularscaffolding.entities.EventType;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventForUserRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventTypeRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.user.UserService;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping({"/api/admin"})
@CrossOrigin(origins = "*")
public class AdminController {
  private final UserService userService;
  private final EventRepository eventRepository;
  private final EventTypeRepository eventTypeRepository;
  private final EventForUserRepository eventForUserRepository ;

  public AdminController(UserService userService, EventRepository eventRepository, EventTypeRepository eventTypeRepository, EventForUserRepository eventForUserRepository) {
    this.userService = userService;
      this.eventRepository = eventRepository;
      this.eventTypeRepository = eventTypeRepository;
    this.eventForUserRepository = eventForUserRepository;
  }

  @GetMapping
  @ResponseBody
  @CrossOrigin(origins = "*")
  public List<Event> getSimpleEventList() {
      return new ArrayList<>(eventRepository.getEvents());
  }

  @GetMapping(path = {"/users/all"})
  public List<User> findAll(){ return userService.findAll();
  }

  @DeleteMapping(path ={"/{id}"})
  public void delete(@PathVariable("id") long id) {
    userService.deleteUserById(id);
    eventForUserRepository.deleteAllForUser(id);
  }

  @PostMapping(path = {"/appointAdmin"})
  public void appointAdmin(@RequestBody String id){
    try{
      long longId = Long.parseLong(id);
      userService.setRoleAdmin(longId);
    } catch (NumberFormatException ex){
        throw new NumberFormatException("User not found!");
    }
  }

    @GetMapping("/eventType")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public  List<EventType> getAllEventTypes() {
        return new ArrayList<>(eventTypeRepository.getAllEventTypes());
    }

//  @GetMapping("/admin/requiredAttributes/{id}")
//  @ResponseBody
//  @CrossOrigin(origins = "*")
//  public  List<Attribute> getRequiredAttributes(@PathVariable String id) {
//    int intId = Integer.parseInt(id);
//    return new ArrayList<>(eventTypeRepository.getRequiredAttributesById(intId));
//  }

    @PostMapping("/addEvent")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public void addEvent(@RequestBody Event event){
        eventRepository.addEvent(event);
    }

  @PostMapping("/editEvent")
  @ResponseBody
  @CrossOrigin(origins = "*")
  public void editEvent(@RequestBody Event event){
    eventRepository.updateEvent(event);
  }

    @DeleteMapping("/deleteEvent/{id}")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public void delete(@PathVariable("id") String id) {
        int intId = Integer.parseInt(id);
        eventForUserRepository.deleteEventForAll(intId);
        eventRepository.deleteEventById(intId);
    }

}
