package in.keepgrowing.jwtspringbootangularscaffolding.api;

import in.keepgrowing.jwtspringbootangularscaffolding.entities.Cities;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.CitiesRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/api/city"})
public class CitiesController {
    private final CitiesRepository citiesRepository ;

    public CitiesController(CitiesRepository citiesRepository) {
        this.citiesRepository = citiesRepository;
    }

    @GetMapping
    public Cities getCities() {
        return citiesRepository.getCities();
    }

    @PostMapping
    public void userCity(@RequestBody String city){
        citiesRepository.addCity(city);
    }
}
