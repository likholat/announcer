package in.keepgrowing.jwtspringbootangularscaffolding.repositories;

import in.keepgrowing.jwtspringbootangularscaffolding.entities.Event;
import in.keepgrowing.jwtspringbootangularscaffolding.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.*;

@Repository
public class EventForUserRepository {
    private final JdbcTemplate jdbcTemplate;
    private final EventRepository eventRepository;
    private final UserService userService ;

    public EventForUserRepository(JdbcTemplate jdbcTemplate, EventRepository eventRepository, UserService userService) {
        this.jdbcTemplate = jdbcTemplate;
        this.eventRepository = eventRepository;
        this.userService = userService;
    }

    public void setEventForUser(Long userId, int eventId){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "insert into user_event (user_id, event_id) VALUES (?,?)"
            );
            ps.setLong(1, userId);
            ps.setInt(2, eventId);
            return ps;
        });
    }

    public void deleteEventForUser(Long userId, int eventId){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "delete from user_event WHERE user_id = ? AND event_id = ?"
            );
            ps.setLong(1, userId);
            ps.setInt(2, eventId);
            return ps;
        });
    }

    public void deleteEventForAll(int eventId){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "delete from user_event WHERE event_id = ?"
            );
            ps.setInt(1, eventId);
            return ps;
        });
    }

    public void deleteAllForUser(Long userId){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "delete from user_event WHERE user_id = ?"
            );
            ps.setLong(1, userId);
            return ps;
        });
    }

    public Set<Event> userEvents(Long userId){
        Set<Event> events = new HashSet<>();
        List<Integer> eventsId = jdbcTemplate.query(
                "select * from user_event where user_id = ?",
                new Object[]{userId},
                (rs, rowNum) -> rs.getInt("event_id"));
        for(Integer i:eventsId){
            events.add(eventRepository.getEventById(i));
        }
        return events;
    }
}
