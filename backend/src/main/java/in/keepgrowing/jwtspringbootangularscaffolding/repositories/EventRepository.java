package in.keepgrowing.jwtspringbootangularscaffolding.repositories;


import in.keepgrowing.jwtspringbootangularscaffolding.entities.*;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.WrongDateFormatException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Repository
public class EventRepository {
    private final JdbcTemplate jdbcTemplate;
    private final EventTypeRepository eventTypeRepository;
    private final AttributeRepository attributeRepository;
    private final ValueRepository valueRepository;

    public EventRepository(EventTypeRepository eventTypeRepository, JdbcTemplate jdbcTemplate, AttributeRepository attributeRepository, ValueRepository valueRepository) {
        this.eventTypeRepository = eventTypeRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.attributeRepository = attributeRepository;
        this.valueRepository = valueRepository;
    }

    public Event getEventById(int id) {
        Event event = jdbcTemplate.queryForObject(
                "select * from events where id = ?",
                new Object[]{id},
                (rs, rowNum) -> {
                    Event eventTemp = new Event();
                    eventTemp.setId(rs.getInt("id"));
                    eventTemp.setName(rs.getString("name"));
                    eventTemp.setEventType(eventTypeRepository.getEventTypeById(rs.getInt("event_type_id")));
                    return eventTemp;
                });

        if (event == null)
            return null;

        if (event.getEventType() == null)
            return null;

        Map<Integer, String> values = new HashMap<>();
        jdbcTemplate.query(
                "select attribute_id from event_type_attribute where event_type_id = ?",
                new Object[]{event.getEventType().getId()},
                rs -> {
                  if(valueRepository.getValueByEventAndAttribute(event,
                    attributeRepository.getAttributeById(rs.getInt("attribute_id")))!= null)
                    values.put(
                            attributeRepository.getAttributeById(rs.getInt("attribute_id")).getId(),
                            valueRepository.getValueByEventAndAttribute(event,
                                    attributeRepository.getAttributeById(rs.getInt("attribute_id")))
                    );
                }
        );
        event.setValuesMap(values);
        return event;
    }

    public void deleteEventById(int id) {
      Event event = getEventById(id);
      if (event == null)
        return;

      if (event.getValuesMap() != null) {
          valueRepository.deleteValuesByEventId(id);
      }

      jdbcTemplate.update("delete from events where id = ?", id);
    }

    public void addEvent(Event event){
      KeyHolder keyHolder = new GeneratedKeyHolder();
      int res = jdbcTemplate.update(con -> {
          PreparedStatement ps = con.prepareStatement(
            "insert into events(name, event_type_id) VALUES (?,?)",
            new String[]{"id"}
            );
          ps.setString(1,event.getName());
          ps.setInt(2,event.getEventType().getId());
          return ps;
        }, keyHolder);
      if(res != 0)
        event.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        valueRepository.addValuesForEvent(event);
    }

    public void updateEvent(Event event){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "update events set name = ?, event_type_id = ? where id = ?"
            );
            ps.setString(1,event.getName());
            ps.setLong(2,event.getEventType().getId());
            ps.setInt(3,event.getId());
            return ps;
        });
        valueRepository.deleteValuesByEventId(event.getId());
        valueRepository.addValuesForEvent(event);
    }

    public Set<Event> getEventsByEventType(EventType eventType){
      Set<Event> events = new HashSet<>();
      jdbcTemplate.query(
        "select id from events where event_type_id = ?",
        new Object[]{eventType.getId()},
        rs -> {
          events.add(this.getEventById(rs.getInt("id")));
        }
      );
      return events;
    }

  public Set<Event> getEvents(){
    Set<Event> events = new HashSet<>();
    jdbcTemplate.query("select id from events where id != 32",
      rs -> {
        events.add(this.getEventById(rs.getInt("id")));
      }
    );
    return events;
  }

    public Set<Event> sortEventsByDate(Set<Event> events){
        Attribute dateAttribute = attributeRepository.getAttributeById(1);
        LocalDate today = LocalDate.now();

        Iterator<Event> iterator = events.iterator();
        while (iterator.hasNext()){
            Event event = iterator.next();
           String strDate = valueRepository.getValueByEventAndAttribute(event,dateAttribute);
           DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy");
           try {
               LocalDate localDate = LocalDate.parse(strDate,format);
               if(localDate.isBefore(today)){
                   iterator.remove();
               }
           } catch (RuntimeException e){
               iterator.remove();
//               throw new WrongDateFormatException();
           }
        }
        return events;
    }

    private Set<Event> getEventsByValue(Integer attribute_id, String value) {
        Set<Event> events = new HashSet<>();
        jdbcTemplate.query(
                "select event_id from values where attribute_id = ? AND value = ? and event_id != ?",
                new Object[]{attribute_id,value,Cities.getId()},
                rs -> {
                    events.add(this.getEventById(rs.getInt("event_id")));
                }
        );
        return events;
    }

    public Set<Event> getEventsByCityAndDate(String city) {
        Set<Event> events = getEventsByValue(2,city);
        sortEventsByDate(events);
        return events;
    }

  public Set<Event> getEventsByAttributes(Map<Integer,String> attributeStringMap){
    Set<Event> events = new HashSet<>();
    for(Map.Entry<Integer,String> entry : attributeStringMap.entrySet()) {
      jdbcTemplate.query("select event_id from values where attribute_id = ? and value = ?",
        new Object[]{entry.getKey(), entry.getValue()},
        rs -> {
          events.add(this.getEventById(rs.getInt("event_id")));
        }
      );
    }
        return events;
  }
}

