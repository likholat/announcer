package in.keepgrowing.jwtspringbootangularscaffolding.repositories;


import in.keepgrowing.jwtspringbootangularscaffolding.entities.Attribute;
import in.keepgrowing.jwtspringbootangularscaffolding.entities.EventType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class EventTypeRepository {
    private final JdbcTemplate jdbcTemplate;
    private final AttributeRepository attributeRepository;


    public EventTypeRepository(JdbcTemplate jdbcTemplate, AttributeRepository attributeRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.attributeRepository = attributeRepository;
    }

    public EventType getEventTypeById(int id) {
        EventType eventType = jdbcTemplate.queryForObject("select * from event_types where id = ?",
                new Object[]{id},
                (rs, rowNum) -> {
                    EventType eventTypeTemp = new EventType();
                    eventTypeTemp.setId(rs.getInt("id"));
                    eventTypeTemp.setName(rs.getString("name"));
                    return eventTypeTemp;
                });

        if (eventType == null)
            return null;

        List<Attribute> attributes =
                jdbcTemplate.query("select attribute_id from event_type_attribute where event_type_id = ?",
                        new Object[]{eventType.getId()},
                        (rs, rowNum) -> attributeRepository.getAttributeById(rs.getInt("attribute_id"))
                );
        eventType.setAttributes(new HashSet<>(attributes));

        Set<Attribute> requiredAttributes = this.getRequiredAttributesById(eventType.getId());
        eventType.setRequiredAttributes(requiredAttributes);

        return eventType;
    }

    public Set<EventType> getAllEventTypes(){
       Set<EventType> eventTypes = new HashSet<>();
       jdbcTemplate.query(
               "select * from event_types where id != 13",
                rs -> {
                   eventTypes.add(this.getEventTypeById(rs.getInt("id")));
                });

      for (EventType eventType: eventTypes) {
        eventType.setRequiredAttributes(this.getRequiredAttributesById(eventType.getId()));
      }

       return eventTypes;
    }

   public Set<Attribute> getRequiredAttributesById(int id){
      Set<Attribute> requiredAttributes = new HashSet<>();
      jdbcTemplate.query(
        "select attribute_id from event_type_attribute where event_type_id = ? and required = true",
        new Object[]{id},
        rs -> {
          requiredAttributes.add(attributeRepository.getAttributeById(rs.getInt("attribute_id")));
        });
      return requiredAttributes;

    }

}
