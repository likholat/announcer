package in.keepgrowing.jwtspringbootangularscaffolding.repositories;

import in.keepgrowing.jwtspringbootangularscaffolding.entities.Attribute;
import in.keepgrowing.jwtspringbootangularscaffolding.entities.Event;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Set;

@Repository
public class ValueRepository {
    private final JdbcTemplate jdbcTemplate;

    public ValueRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    String getValueByEventAndAttribute(Event event, Attribute attribute) {
        return jdbcTemplate.query(
                "select value from values where event_id = ? and attribute_id = ?",
                new Object[]{event.getId(), attribute.getId()},
                (rs, rowNum) -> rs.getString("value")
        ).stream().findAny().orElse(null);
    }


    String deleteValuesByEventId(int eventId) {
      int update = jdbcTemplate.update("delete from values where event_id = ?", eventId);
      if (update == 0) {
        return "Failed";
      } else {
        return "SUCCESS";
      }
    }

    void addValuesForEvent(Event event){
      for(Map.Entry<Integer,String> value: event.getValuesMap().entrySet()) {
        jdbcTemplate.update("insert into values (event_id, attribute_id, value) VALUES (?,?,?)",
          event.getId(), value.getKey(),value.getValue());
      }
    }
}
