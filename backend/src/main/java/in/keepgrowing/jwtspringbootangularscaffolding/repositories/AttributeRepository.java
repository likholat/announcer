package in.keepgrowing.jwtspringbootangularscaffolding.repositories;


import in.keepgrowing.jwtspringbootangularscaffolding.entities.Attribute;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class AttributeRepository {
    private final JdbcTemplate jdbcTemplate;

    public AttributeRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //prepared statement!!!!!!!!!!

    public Attribute getAttributeById(int id){
        return jdbcTemplate.queryForObject(
                "select * from attributes where id = ?",
                new Object[]{id},
                (rs, rowNum) -> {
                    Attribute attribute = new Attribute();
                    attribute.setId(rs.getInt("id"));
                    attribute.setName(rs.getString("name"));
                    return attribute;
                }
        );
    }


    public Set<Attribute> getAttributesByEventTypeId(int id){
       List<Integer> attributesId = jdbcTemplate.query(
               "select * from event_type_attribute where event_type_id = ?",
                new Object[]{id},
                (rs, rowNum) -> rs.getInt("attribute_id"));

       Set<Attribute> attributes = new HashSet<>();
       for(Integer i:attributesId){
           attributes.add(getAttributeById(i));
       }
       return attributes;
    }
}
