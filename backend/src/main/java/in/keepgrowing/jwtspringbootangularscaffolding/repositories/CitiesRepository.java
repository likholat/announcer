package in.keepgrowing.jwtspringbootangularscaffolding.repositories;

import in.keepgrowing.jwtspringbootangularscaffolding.entities.Cities;
import in.keepgrowing.jwtspringbootangularscaffolding.entities.Event;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Repository
public class CitiesRepository {
    private final JdbcTemplate jdbcTemplate;

    public CitiesRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Cities getCities() {
        Cities cities = new Cities();
        Set<String> names = new HashSet<>();

        jdbcTemplate.query(
                    "select * from values where event_id = ?",
                    new Object[]{Cities.getId()},
                    (rs) -> {
                        names.add(rs.getString("value"));
                    }
            );
            cities.setCityNames(names);
            return cities;
    }

    public void addCity(String city){

        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "insert into values (event_id, attribute_id, value) values (?,?,?)"
            );
            ps.setInt(1, Cities.getId());
            ps.setInt(2, Cities.getAttributeCityId());
            ps.setString(3, city);
            return ps;
        });
    }
}
