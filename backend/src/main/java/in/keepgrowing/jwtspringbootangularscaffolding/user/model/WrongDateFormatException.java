package in.keepgrowing.jwtspringbootangularscaffolding.user.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Wrong date format")
public class WrongDateFormatException extends RuntimeException {
}
