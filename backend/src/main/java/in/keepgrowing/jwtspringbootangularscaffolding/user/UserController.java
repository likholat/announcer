package in.keepgrowing.jwtspringbootangularscaffolding.user;

import in.keepgrowing.jwtspringbootangularscaffolding.entities.Event;
import in.keepgrowing.jwtspringbootangularscaffolding.repositories.EventForUserRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.User;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.UserCredentials;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.UserInformationRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {
    private UserService userService;
    private final EventForUserRepository eventForUserRepository;
    private final UserInformationRepository userInformationRepository;

    public UserController(UserService userService, EventForUserRepository eventForUserRepository, UserInformationRepository userInformationRepository) {
        this.userService = userService;
        this.eventForUserRepository = eventForUserRepository;
        this.userInformationRepository = userInformationRepository;
    }

//    @PostMapping
//    public User register(@RequestBody User user) {
//        return userService.register(user);
//    }

    @PostMapping
    public User register(@RequestBody UserCredentials userCredentials) {
        return userService.register(new User(userCredentials));
    }

    @PostMapping("/like")
    public void addEventForUser(@RequestBody String eventId){
        Long userId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try{
            int intEventId = Integer.parseInt(eventId);
            if (principal instanceof UserDetails) {
                String username = ((UserDetails) principal).getUsername();
                if (userService.findByUsername(username).isPresent()) {
                    userId = userService.findByUsername(username).get().getId();
                    eventForUserRepository.setEventForUser(userId, intEventId);
                }
            }
        } catch (NumberFormatException ex){
            throw new NumberFormatException("Error with event id!");
        }
    }

    @PostMapping("/dislike")
    public void dislike(@RequestBody String eventId){
        Long userId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try{
            int intEventId = Integer.parseInt(eventId);
            if (principal instanceof UserDetails) {
                String username = ((UserDetails) principal).getUsername();
                if (userService.findByUsername(username).isPresent()) {
                    userId = userService.findByUsername(username).get().getId();
                    eventForUserRepository.deleteEventForUser(userId, intEventId);
                }
            }
        } catch (NumberFormatException ex){
            throw new NumberFormatException("Error with event id!");
        }
    }

    @GetMapping("/userPage")
    @CrossOrigin(origins = "*")
    public List<Event> getUserEventList() {
        Long userId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails) {
                String username = ((UserDetails) principal).getUsername();
                if (userService.findByUsername(username).isPresent()) {
                    userId = userService.findByUsername(username).get().getId();
                    return new ArrayList<>(eventForUserRepository.userEvents(userId));
                }
            }
        return new ArrayList<>();
    }

    @PostMapping("/email")
    public void userMail(@RequestBody String email){
        Long userId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            if (userService.findByUsername(username).isPresent()) {
                userId = userService.findByUsername(username).get().getId();
                userInformationRepository.updateMail(userId,email);
            }
        }
    }

    @PostMapping("/city")
    public void userCity(@RequestBody String city){
        Long userId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            if (userService.findByUsername(username).isPresent()) {
                userId = userService.findByUsername(username).get().getId();
                userInformationRepository.updateCity(userId,city);
            }
        }
    }

    @GetMapping("/userInfo")
    @ResponseBody
    public UserCredentials getUserInfo() {
        UserCredentials userInfo = new UserCredentials();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            userInfo.setUsername(((UserDetails) principal).getUsername());
            if (userService.findByUsername(userInfo.getUsername()).isPresent()) {
                Long userId = userService.findByUsername(userInfo.getUsername()).get().getId();
                userInfo.setCity(userInformationRepository.getCity(userId));
                userInfo.setEmail(userInformationRepository.getEmail(userId));
            }
        }
        return userInfo;
    }


}
