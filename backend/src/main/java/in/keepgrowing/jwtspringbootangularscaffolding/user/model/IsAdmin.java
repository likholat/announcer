package in.keepgrowing.jwtspringbootangularscaffolding.user.model;


import lombok.Data;

@Data
public class IsAdmin {
    private boolean isAdmin;

    public IsAdmin(){}
    public IsAdmin(boolean isAdmin){
        this.isAdmin = isAdmin;
    }

}
