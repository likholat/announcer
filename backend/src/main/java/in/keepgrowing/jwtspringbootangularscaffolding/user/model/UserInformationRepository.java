package in.keepgrowing.jwtspringbootangularscaffolding.user.model;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;

@Repository
public class UserInformationRepository {
    private final JdbcTemplate jdbcTemplate;

    public UserInformationRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void updateMail(Long userId, String email){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "update my_user set email = ? where id = ?"
            );
            ps.setString(1,email);
            ps.setLong(2,userId);
            return ps;
        });
    }

    public void updateCity(Long userId, String city){
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(
                    "update my_user set city = ? where id = ?"
            );
            ps.setString(1,city);
            ps.setLong(2,userId);
            return ps;
        });
    }

    public String getCity(Long userId){
       try {
           return jdbcTemplate.query(
                   "select city from my_user where id = ?",
                   new Object[]{userId},
                   (rs, rowNum) -> rs.getString("city")
           ).stream().findAny().orElse(null);
       } catch (Exception e){
           return "";
       }
    }

    public String getEmail(Long userId){
        try {
            return jdbcTemplate.query(
                    "select email from my_user where id = ?",
                    new Object[]{userId},
                    (rs, rowNum) -> rs.getString("email")
            ).stream().findAny().orElse(null);
        } catch (Exception e){
            return "";
        }
    }
}
