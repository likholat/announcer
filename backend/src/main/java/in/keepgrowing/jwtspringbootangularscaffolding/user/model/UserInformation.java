package in.keepgrowing.jwtspringbootangularscaffolding.user.model;

import lombok.Data;

@Data
public class UserInformation {
    private String login;
    private String email;
    private String city;
}
