package in.keepgrowing.jwtspringbootangularscaffolding.user;


import in.keepgrowing.jwtspringbootangularscaffolding.user.model.User;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.UserRepository;
import in.keepgrowing.jwtspringbootangularscaffolding.user.model.UsernameIsntUniqueException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    private static final String DEFAULT_ROLE = "ROLE_USER";
    private static final String ADMIN_ROLE = "ROLE_ADMIN";
    private UserRepository userRepository;
    private BCryptPasswordEncoder encoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public User register(User user) {
        setPasswordAndRole(user);
        try {
         return userRepository.save(user);
        } catch (RuntimeException e){
            throw new UsernameIsntUniqueException();
        }
    }

    private void setPasswordAndRole(User user) {
        user.getUserCredentials()
                .setPassword(encoder.encode(user.getUserCredentials().getPassword()));
        user.getUserCredentials().setRole(DEFAULT_ROLE);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUserCredentialsUsername(username);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    public void setRoleAdmin(Long id) {
        Optional<User> user = userRepository.findUserById(id);
        if(user.isPresent()){
            User u = user.get();
            u.getUserCredentials().setRole(ADMIN_ROLE);
            userRepository.save(u);
        }
    }

}