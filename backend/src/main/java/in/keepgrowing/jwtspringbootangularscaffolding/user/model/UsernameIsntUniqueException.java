package in.keepgrowing.jwtspringbootangularscaffolding.user.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NETWORK_AUTHENTICATION_REQUIRED, reason = "Login is not unique")
public class UsernameIsntUniqueException extends RuntimeException {
}
