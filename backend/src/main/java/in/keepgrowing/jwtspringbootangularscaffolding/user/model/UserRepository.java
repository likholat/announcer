package in.keepgrowing.jwtspringbootangularscaffolding.user.model;

import org.springframework.data.repository.CrudRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUserCredentialsUsername(String username);
    Optional<User> findUserById(Long id);
    void delete(User user);

    @Override
    void deleteById(Long aLong);

    List<User> findAll();
    User save(User user);
}
