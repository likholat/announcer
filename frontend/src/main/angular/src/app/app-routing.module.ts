import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/guards/auth.guard';
import { LoginComponent } from './auth/components/login/login.component';
import {EventsComponent} from "./events/events.component";
import {EventDetailesComponent} from "./event-detailes/event-detailes.component";
import {RegisterComponent} from "./register/register.component";
import {AdminEventsComponent} from "./admin-events/admin-events.component";
import {AdminUsersComponent} from "./admin-users/admin-users.component";
import {AddEventComponent} from "./add-event/add-event.component";
import {UserPageComponent} from "./user-page/user-page.component";
import {EditEventComponent} from "./edit-event/edit-event.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent},
  {path:'notFound', component: PageNotFoundComponent},
  { canActivate: [AuthGuard], path: 'event', component: EventsComponent },
  { canActivate: [AuthGuard], path: 'event/:eventId', component: EventDetailesComponent },
  { canActivate: [AuthGuard], path: 'admin/event', component: AdminEventsComponent },
  { canActivate: [AuthGuard], path: 'admin/users', component: AdminUsersComponent },
  { canActivate: [AuthGuard], path: 'admin/users', component: AdminUsersComponent },
  { canActivate: [AuthGuard], path: 'admin/addEvent', component: AddEventComponent },
  { canActivate: [AuthGuard], path: 'user', component: UserPageComponent },
  { canActivate: [AuthGuard], path: 'admin/editEvent/:eventId', component: EditEventComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
