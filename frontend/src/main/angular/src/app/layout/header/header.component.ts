import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }

  public logout():void {
    this.authService.logout();
  }

  events():void{
    this.router.navigate(["/event"]).then();
  }

  isAdmin():boolean{
    if(localStorage.getItem('admin')=='false'){
      return false;
    }
    if(localStorage.getItem('admin')=='true'){
     return true;
    }
    return false;
  }

}
