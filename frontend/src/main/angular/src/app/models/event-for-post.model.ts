import {EventType} from "./event-type.model";
import {MyEvent} from "./my-event.model";

export class EventForPost{
  id: string;
  name: string;
  eventType: EventType;
  valuesMap:{} = null;
}
