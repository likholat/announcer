import {EventType} from "./event-type.model";

export class MyEvent{
  id: string;
  name: string;
  eventType: EventType;
  valuesMap: Map<number,string> = new Map<number, string>();
}
