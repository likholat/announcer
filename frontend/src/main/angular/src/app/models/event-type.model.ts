import {Attribute} from "./attribute.model";

export class EventType{
  public id: string;
  public name: string;
  public attributes: Set<Attribute>;
  public requiredAttributes: Set<Attribute>;
}

