import { Injectable } from '@angular/core';

import { AuthService } from '../services/auth.service';
import {HttpHandler, HttpRequest, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from "rxjs/operators";
import {Router} from "@angular/router";
import {TokenService} from "../services/token.service";
import {RegisterComponent} from "../../register/register.component";

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService, private router: Router, public tokenService: TokenService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let interceptedRequest = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`
      }
    });

    return next.handle(interceptedRequest).pipe(catchError(x => this.handleErrors(x)));
  }

  private handleErrors(err: HttpErrorResponse): Observable<any> {
    if (err.status === 401 || err.status === 403) {
      this.auth.redirectToUrl = this.router.url;
      this.router.navigate(['/login']).then();
      return of(err.message);
    }

    if(err.status === 500) {
      this.router.navigate(['/notFound']).then();
      return of(err.message);
    }

    if(err.status === 511){
      //localStorage.setItem('loginError','Логин занят, выберите другой');
      this.auth.registrationErrMsg = 'Логин занят, выберите другой';
      return of(err.message);
    }
  }

}
