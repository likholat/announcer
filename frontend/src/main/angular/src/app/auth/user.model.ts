import {Credentials} from "./credentials";

export class User{
  id: string;
  userCredentials: Credentials;
}
