import {Attribute} from "../../models/attribute.model";
import {EventType} from "../../models/event-type.model";

export function containRequired(eventType: EventType, attribute: Attribute): boolean{
  for(let item of eventType.requiredAttributes)
    if(attribute.id == item.id)
      return true;
  return false;
}

export function getAttributeName(eventType: EventType, id: number): string{
  for(let item of eventType.attributes)
    if(id == item.id)
      return item.name;
  return '';
}

// export function setNew(eventType: EventType):void{
//   this.id = eventType.id;
// this.name = eventType.name;
// this.requiredAttributes = eventType.requiredAttributes;
// this.attributes = eventType.attributes;
// }
