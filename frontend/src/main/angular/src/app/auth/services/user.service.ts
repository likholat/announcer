import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from "../../../environments/environment";
import {User} from "../user.model";




const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

// const API_URL = environment.apiUrl;
const ADMIN_API_URL = environment.adminApiUrl;

@Injectable()
export class UserService {

  constructor(private http:HttpClient) {}

  // private adminUrl = `http://localhost:9000/api/admin`;

  public getUsers() {
    let url = ADMIN_API_URL + '/users/all';
    return this.http.get<Array<User>>(url);
  }

  public deleteUser(user) {
    return this.http.delete(ADMIN_API_URL + `/${user.id}`);
  }

  public appointAdmin(user){
    return this.http.post(ADMIN_API_URL + `/appointAdmin`, user.id);
  }

}
