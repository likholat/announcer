import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Credentials } from '../credentials';
import {Observable} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  observe: 'response' as 'response'
};
const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  isAdmin: boolean;
  errorMessage: string = "";

  constructor(private http: HttpClient) {
    this.isAdmin = false;
  }

  public getResponseHeaders(userCredentials: Credentials) {
    let loginUrl = API_URL + '/login';
    return this.http.post(loginUrl, userCredentials, httpOptions);
  }

  public create(credentials: Credentials) : Observable<Object>
  {
    let usersUrl = API_URL + '/users';
    return this.http.post<Credentials>(usersUrl, credentials, httpOptions);
  }

  public logout() {
    let logoutUrl = API_URL + '/logout';
    return this.http.get(logoutUrl, {responseType: 'text'});
  }
}
