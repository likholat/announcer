import { Injectable } from '@angular/core';

import { TokenService } from './token.service'
import { Credentials } from '../credentials';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Router } from '@angular/router';
import {environment} from "../../../environments/environment";

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static readonly TOKEN_STORAGE_KEY = 'token';
  static readonly IS_ADMIN_STORAGE_KEY = 'admin';
  redirectToUrl: string = '/event';
  registrationErrMsg: string = '';

  constructor(private router: Router, private tokenService: TokenService,private http: HttpClient) { }

  public login(credentials: Credentials): void {
    this.tokenService.getResponseHeaders(credentials)
    .subscribe((res: HttpResponse<any>) => {
      this.saveToken(res.headers.get('authorization'));
      this.go().then();
    });
  }

  public async go(){
      let loginUrl = API_URL + '/roleAdmin';
      let admin = await this.http.get<boolean>(loginUrl).toPromise();
      this.saveRole(String(admin));
      if (!admin) {
        this.router.navigate(["/event"]).then();
      }
      else if (admin) {
        this.router.navigate(["/admin/event"]).then();
      }
      else
        console.log("ROLE ERROR");
  }

  public create(credentials: Credentials): void {
    this.tokenService.create(credentials)
      .subscribe((res: HttpResponse<any>) => {
        this.router.navigate(['/login']).then(()=> {
        });
      });
  }


  private saveToken(token: string){
    localStorage.setItem(AuthService.TOKEN_STORAGE_KEY, token);
  }

  private saveRole(role: string){
    localStorage.setItem(AuthService.IS_ADMIN_STORAGE_KEY, role);
  }

  public getToken(): string {
    return localStorage.getItem(AuthService.TOKEN_STORAGE_KEY);
  }

  public logout(): void {
    this.tokenService.logout()
    .subscribe(() =>{
      localStorage.removeItem(AuthService.IS_ADMIN_STORAGE_KEY);
      localStorage.removeItem(AuthService.TOKEN_STORAGE_KEY);
    });
  }

  public aboutError(){
    return this.tokenService.errorMessage;
  }

  public isLoggedIn(): boolean {
    return !!this.getToken();
  }
}
