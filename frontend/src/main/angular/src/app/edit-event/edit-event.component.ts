import {Component, Inject, OnInit} from '@angular/core';
import {MyEvent} from "../models/my-event.model";
import {DOCUMENT} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {EventForPost} from "../models/event-for-post.model";
import {EventType} from "../models/event-type.model";
import {CitiesModel} from "../models/cities.model";
import {Attribute} from "../models/attribute.model";
import {containRequired} from "../auth/services/event_type.service";
import {convertEvent} from "../auth/services/event.service";

const API_URL = environment.apiUrl;
const ADMIN_API_URL = environment.adminApiUrl;

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})

export class EditEventComponent implements OnInit {
  event: MyEvent = new MyEvent();
  newEvent: EventForPost = new EventForPost();
  eventTypes: Array<EventType>;
  cities: CitiesModel = new CitiesModel();
  selectedCity: String = '';


  constructor(
    @Inject(DOCUMENT) private document: Document,
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
  ) { }

  ngOnInit() {
    this.event.eventType = new EventType();

    this.route.paramMap.subscribe(params => {
      this.http.get<MyEvent>(API_URL +`/event/${params.get('eventId')}`)
        .subscribe(myEvent => this.event = myEvent);
    });

    this.http.get<Array<EventType>>(ADMIN_API_URL + `/eventType`)
      .subscribe(eventTypes => this.eventTypes = eventTypes);

    this.http.get<CitiesModel>(API_URL + `/city`).subscribe(res=> this.cities = res);
  }


  setType(){
    this.event.valuesMap = new Map<number, string>();
    for(let i = 0; i < this.eventTypes.length; i++){
      if(this.eventTypes[i].name == this.event.eventType.name){
        this.event.eventType = this.eventTypes[i];
        return;
      }
    }
    if(this.event.eventType.id==null)
      console.log("ERROR WITH EVENT_TYPE");

  }

  addValue(attributeId,inputValue){
    this.event.valuesMap.set(attributeId, inputValue);
  }

  containRequired(eventType: EventType, attribute: Attribute) {
    return containRequired(eventType,attribute);
  }

  saveEvent(): void {
   // convertEvent(this.event,this.newEvent);
    let addUrl = ADMIN_API_URL + '/editEvent';
    this.http.post(addUrl,this.event).subscribe();
    alert('Сохранено')
    //this.router.navigate(["/admin/event"]);
  }

}
