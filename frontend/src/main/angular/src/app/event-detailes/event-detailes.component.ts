import {Component, Inject, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpClient, HttpParams} from "@angular/common/http";
import { DOCUMENT } from '@angular/common';
import {MyEvent} from "../models/my-event.model";
import {environment} from "../../environments/environment";
import {getAttributeName} from "../auth/services/event_type.service";
import {EventType} from "../models/event-type.model";

const API_URL = environment.apiUrl;

@Component({
  selector: 'app-event-detailes',
  templateUrl: './event-detailes.component.html',
  styleUrls: ['./event-detailes.component.css']
})
export class EventDetailesComponent implements OnInit {

  myEvent: MyEvent = new MyEvent();
  //isUrl: boolean;
  urlString: string = '';

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private route: ActivatedRoute,
    private http: HttpClient,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.http.get<MyEvent>(API_URL +`/event/${params.get('eventId')}`)
        .subscribe(myEvent => this.myEvent = myEvent);
    });
  }

  isUrl():boolean{
    return (this.myEvent.valuesMap[8] != null) &&(this.myEvent.valuesMap[8] !='');
  }

  goToUrl(): void {
    if(this.myEvent.valuesMap[8]!=null)
      this.urlString = this.myEvent.valuesMap[8];
    else return;
    if(this.urlString!='')
      window.location.href = this.urlString;
    else return;
  }


  getAttributeName(eventType: EventType, id: number) {
    return getAttributeName(eventType, id);
  }
}
