import { Component, OnInit } from '@angular/core';
import {MyEvent} from "../models/my-event.model";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Credentials} from "../auth/credentials";
import {Observable} from "rxjs";
import {CitiesModel} from "../models/cities.model";

const API_URL = environment.apiUrl;
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  simpleEventList: Array<MyEvent> = new Array<MyEvent>();
  credentials: Credentials = new Credentials('','');
  cities: CitiesModel = new CitiesModel();

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<Array<MyEvent>>(API_URL + '/users/userPage').subscribe(simpleEventList=>
      this.simpleEventList = simpleEventList
    );
    this.credentials.email = '';
    this.credentials.city = '';
    this.http.get<Credentials>(API_URL +`/users/userInfo`).subscribe(res=> this.credentials = res);
    this.http.get<CitiesModel>(API_URL + `/city`).subscribe(res=> this.cities = res);
  }

  public dislikeIt(eventId: string){
    this.http.post(API_URL + `/users/dislike`,eventId).subscribe();
  }

  public updateUserInformation(){
    if(this.credentials.email != ''){
      this.http.post(API_URL + `/users/email`,this.credentials.email).subscribe();
    }
    if(this.credentials.city != ''){
      this.http.post(API_URL + `/users/city`,this.credentials.city).subscribe();
    }
    alert("Данные сохранены!");
  }



}
