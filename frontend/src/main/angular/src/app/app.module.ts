import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtTokenInterceptor } from './auth/interceptors/jwt.token.interceptor';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import {EventsComponent} from "./events/events.component";
import {EventDetailesComponent} from "./event-detailes/event-detailes.component";
import {RegisterComponent} from "./register/register.component";
import {AdminEventsComponent} from "./admin-events/admin-events.component";
import {AdminUsersComponent} from "./admin-users/admin-users.component";
import {UserService} from "./auth/services/user.service";
import {AddEventComponent} from "./add-event/add-event.component";
import {NgSelectModule} from "@ng-select/ng-select";
import { UserPageComponent } from './user-page/user-page.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    HeaderComponent,
    EventsComponent,
    EventDetailesComponent,
    RegisterComponent,
    AdminEventsComponent,
    AdminUsersComponent,
    AddEventComponent,
    UserPageComponent,
    EditEventComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgSelectModule, FormsModule
],
  providers: [
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
