import { Component, OnInit } from '@angular/core';

import {AuthService} from "../auth/services/auth.service";
import {Credentials} from "../auth/credentials";
import {__await} from "tslib";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  credentials: Credentials = new Credentials('','');
  oldLogin: string = '';

  constructor(private authService: AuthService) { }

    ngOnInit() {
  }

  createUser(): void {
    localStorage.setItem('loginError','');
    this.oldLogin = this.credentials.username;
    this.authService.create(this.credentials);
  }

  getRegisterStatus():string{
    return this.authService.registrationErrMsg;
  }


}
