import { Component, OnInit } from '@angular/core';
import {MyEvent} from "../models/my-event.model";
import {EventType} from "../models/event-type.model";
import {HttpClient} from "@angular/common/http";
import {EventForPost} from "../models/event-for-post.model";
import {AuthService} from "../auth/services/auth.service";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";
import {CitiesModel} from "../models/cities.model";
import {containRequired} from "../auth/services/event_type.service";
import {Attribute} from "../models/attribute.model";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {DateValidator} from "../validators/date.validator";

const API_URL = environment.apiUrl;
const ADMIN_API_URL = environment.adminApiUrl;

@Component({
  selector: 'app-register',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css'],
})
export class AddEventComponent implements OnInit {

  event: MyEvent = new MyEvent();
  eventTypes: Array<EventType>;
  cities: CitiesModel;
  newCity: String = '';

  constructor(
              private authService: AuthService,
              private http: HttpClient,
              private router: Router,
  ) { }

    ngOnInit() {
      this.http.get<Array<EventType>>(ADMIN_API_URL + `/eventType`)
        .subscribe(eventTypes => this.eventTypes = eventTypes);
      this.event.eventType = new EventType();
      this.http.get<CitiesModel>(API_URL + `/city`).subscribe(res=> this.cities = res);

  }

  getData(){
    let data = this.eventTypes;
    console.log('eventTypes: ',data);
  }

  setType(){
    this.event.valuesMap = new Map<number, string>();
    for(let i = 0; i < this.eventTypes.length; i++){
      if(this.eventTypes[i].name == this.event.eventType.name){
        this.event.eventType=this.eventTypes[i];
        return;
      }
    }
    if(this.event.eventType.id==null)
      console.log("ERROR WITH EVENT_TYPE");

  }

  addEvent(): void {
    let addUrl = ADMIN_API_URL + '/addEvent';
    this.http.post(addUrl,this.event).subscribe();
    this.router.navigate(["/admin/event"]);
  }

  public addCity(){
    this.http.post(API_URL + `/city`,this.newCity).subscribe();
    window.location.reload();
  }

  public aboutUser(){
    window.location.href = "http://localhost:4200/aboutUser";
  }

  public logout(): void{
    try {
      this.authService.logout();
    } catch (e){}

  }

  containRequired(eventType: EventType, attribute: Attribute) {
    return containRequired(eventType,attribute);
  }
}
