import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {MyEvent} from "../models/my-event.model";
import {environment} from "../../environments/environment";
import {EventForPost} from "../models/event-for-post.model";

const API_URL = environment.apiUrl;

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})

export class EventsComponent implements OnInit {

  simpleEventList: Array<MyEvent> = new Array<MyEvent>();
  userEventList: Array<MyEvent> = new Array<MyEvent>();

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.http.get<Array<MyEvent>>(API_URL + '/event/byCity').subscribe(simpleEventList=>
      this.simpleEventList = simpleEventList
    );

    this.http.get<Array<MyEvent>>(API_URL + '/users/userPage').subscribe(userEventList=>
      this.userEventList = userEventList
    );
  }

  public likeIt(eventId: string){
   this.http.post(API_URL + `/users/like`, eventId).subscribe();
  }

  public isLiked(eventId: string):boolean{

    const liked = this.userEventList;
    for(let i = 0; i < liked.length; i++) {
      if (liked[i].id == eventId) {
        return true;
      }
    }
    return false;
  }




}
