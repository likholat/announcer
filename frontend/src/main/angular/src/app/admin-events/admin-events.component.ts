import {Component, OnInit} from '@angular/core';
import {MyEvent} from "../models/my-event.model";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {environment} from "../../environments/environment";
import {AuthService} from "../auth/services/auth.service";

const API_URL = environment.apiUrl;
const ADMIN_API_URL = environment.adminApiUrl;
@Component({
  selector: 'app-admin-events',
  templateUrl: './admin-events.component.html',
  styleUrls: ['./admin-events.component.css']
})
export class AdminEventsComponent implements OnInit {


  simpleEventList: Array<MyEvent>;

  constructor(private http: HttpClient,
              public authService: AuthService,
              private route: ActivatedRoute,) {
  }

  ngOnInit() {
    this.http.get<Array<MyEvent>>(ADMIN_API_URL).subscribe(simpleEventList =>
      this.simpleEventList = simpleEventList
    );
  }

  public logout(): void {
    try {
      this.authService.logout();
    } catch (e) {
    }

  }

  public deleteEvent(eventId: String) {
    // const body = new HttpParams().set("id",id)
      this.http.delete(ADMIN_API_URL + `/deleteEvent/${eventId}`).subscribe();
      window.location.reload();
  }

  public addEvent(){
    window.location.href = 'http://localhost:4200/admin/addEvent';
  }
}
